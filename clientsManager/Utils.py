from datetime import datetime


def print_to_log(message):
    print("{} - {}".format(datetime.now(), message))