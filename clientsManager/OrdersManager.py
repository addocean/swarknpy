from queue import Queue
import time


class OrdersManager:
    def __init__(self):
        self.has_connection = False
        self.orders = Queue()

    def read_orders(self):
        while True:
            if self.orders.empty() and self.has_connection:
                time.sleep(1)
                order = input("Orden a enviar: ")
                self.orders.put(order)
                time.sleep(1)

    def get_new_order(self):
        return self.orders.get()
