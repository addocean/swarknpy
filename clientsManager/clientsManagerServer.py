import socket
import _thread
import time
import constants
import Utils
from OrdersManager import OrdersManager


class ClientsManagerServer:
    def __init__(self, orders_manager_):
        self.orders_manager = orders_manager_

        serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        serversocket.bind(("0.0.0.0", constants.PORT))
        serversocket.listen(1)

        while True:
            Utils.print_to_log("Esperando conexión")
            (clientsocket, address) = serversocket.accept()
            _thread.start_new_thread(self.client_thread, (clientsocket, address))

    def client_thread(self, sock, address):
        self.orders_manager.has_connection = True
        Utils.print_to_log("Nuevo cliente conectado desde {}".format(address))

        index = 0
        while True:
            index += 1

            new_order = orders_manager.get_new_order()

            sock.send("{}\n".format(new_order).encode("utf-8"))
            received = self.myreceive(sock)
            Utils.print_to_log("Recibido {}".format(received.decode("utf-8")))

            if index > 10:
                sock.close()
                break

    @staticmethod
    def myreceive(sock):
        chunks = []
        while True:
            chunk = sock.recv(1)
            if chunk == b'':
                raise RuntimeError("socket connection broken")
            chunks.append(chunk)
            if chunk == b'\n':
                break
        return b''.join(chunks)


def create_clients_manager(orders_manager_):
    ClientsManagerServer(orders_manager_)


orders_manager = OrdersManager()
_thread.start_new_thread(create_clients_manager, (orders_manager, ))
time.sleep(1)
orders_manager.read_orders()
