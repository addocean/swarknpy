import socket
import time
import constants
import Utils


class ClientsManagerClient:
    def __init__(self):
        self.seconds_to_wait_for_server = 10
        self.sock = None

        self.make_connection()

    def make_connection(self):
        while True:
            try:
                self.sock = socket.create_connection(("192.168.17.82", constants.PORT), 1000)
                Utils.print_to_log("Conectado al servidor")
                self.seconds_to_wait_for_server = 10

                while True:
                    received = self.process_connection().decode("utf-8")[:-1]
                    time.sleep(1)
                    Utils.print_to_log("Recibido {}".format(received))
                    result = self.process_order(received)
                    self.sock.send("{} {}\n".format(result, received).encode("utf-8"))

            except:
                Utils.print_to_log("Sin conexión con el servidor. Esperando {} segundos".format(self.seconds_to_wait_for_server))
                time.sleep(self.seconds_to_wait_for_server)
                self.seconds_to_wait_for_server = min(self.seconds_to_wait_for_server * 2, 60*5)

    def process_connection(self):
        chunks = []
        while True:
            chunk = self.sock.recv(1)
            if chunk == b'':
                raise RuntimeError("Conexión Rota!!")
            chunks.append(chunk)
            if chunk == b'\n':
                break
        return b''.join(chunks)

    def process_order(self, received):
        result = "KO"
        if received.upper().startswith("VNC"):
            host = received.split(" ")[1]
            application = "ConnectVNCToADDocean.bat"
            byte_message = bytes("E@" + host + ":" + application, "utf-8")
            opened_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            opened_socket.sendto(byte_message, ("192.168.13.255", 20227))
            result = "OK"

        return result


ClientsManagerClient()
